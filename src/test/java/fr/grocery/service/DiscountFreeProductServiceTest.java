package fr.grocery.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.grocery.model.Basket;
import fr.grocery.model.BasketLine;
import fr.grocery.model.DiscountFreeProduct;
import fr.grocery.model.Product;

public class DiscountFreeProductServiceTest {
	
    /**
     * Margin error for the comparison of double typed values.
     */
    private static final double DELTA = 0.000001;
    
    @Test
    public void testcomputeBasketPrice() {
    	
    	// create Products
        Product apple = new Product ("Apple", 0.20);
        Product orange = new Product("Orange", 0.50);
        Product waterMelon = new Product("WaterMelon", 0.80);
    	
    	
    	// create Discounts
        DiscountFreeProduct discountOneGetOneFree = new DiscountFreeProduct ("Un achete Un gratuit",1,1);
        DiscountFreeProduct discountTwoGetOneFree = new DiscountFreeProduct ("Deux achetes Un gratuit",2,1);        
        
    	
        // Set Discount to Products
        apple.setDiscount(discountOneGetOneFree);
        waterMelon.setDiscount(discountTwoGetOneFree);
        
        
    	// create BasketLines
        
        BasketLine appleBasketLine = new BasketLine (apple, 4); 
        BasketLine orangeBasketLine = new BasketLine (orange, 3); 
        BasketLine waterMelonBasketLine = new BasketLine (waterMelon, 5); 
        
    	// create Basket
        Basket basket = new Basket();
        basket.getBasketLines().add(appleBasketLine);
        basket.getBasketLines().add(orangeBasketLine);
        basket.getBasketLines().add(waterMelonBasketLine);
        
        //simulate basket price
        
        double expectedPriceBasket = 5.1; // 0.4 (apple) + 3.2(waterMelon) + 1.5 (Orange) 
        
        BasketService basketService = new BasketService(basket);
        double computedPriceBasket = basketService.computeBasketPrice();        
        
        
        assertEquals(expectedPriceBasket, computedPriceBasket, DELTA);

        
    }

    

}
