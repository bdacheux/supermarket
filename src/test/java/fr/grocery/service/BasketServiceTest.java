package fr.grocery.service;

import fr.grocery.model.Basket;
import fr.grocery.model.BasketLine;
import fr.grocery.model.Discount;
import fr.grocery.model.DiscountFreeProduct;
import fr.grocery.model.Product;



import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BasketServiceTest {

    /**
     * Margin error for the comparison of double typed values.
     */
    private static final double DELTA = 0.000001;

    @Test
    public void testcomputeBasketPriceWithNoDiscount() {
    	
        Product apple = new Product ("Apple", 0.20);
        Product orange = new Product("Orange", 0.50);
        Product waterMelon = new Product("WaterMelon", 0.80);
        
        Basket basket = new Basket();
        
        basket.getBasketLines().add(new BasketLine(apple,4));
        basket.getBasketLines().add(new BasketLine(orange,3));
        basket.getBasketLines().add(new BasketLine(waterMelon,5));
        

        BasketService basketService = new BasketService(basket);

        double returnPrice = basketService.computeBasketPrice(); 

        // Assert
        double expectedPrice = 6.3; // 4*0.2 + 3*0.5 + 5*0.8
        assertEquals(expectedPrice, returnPrice, DELTA);
        
    }


    @Test
    public void testcomputeBasketPriceWithDiscount() {
    	
        Product apple = new Product ("Apple", 0.20);
        Product orange = new Product("Orange", 0.50);
        Product waterMelon = new Product("WaterMelon", 0.80);
        
        Discount discountOneGetOneFree = new DiscountFreeProduct ("Un achete Un gratuit",1,1);
        Discount discountTwoGetOneFree = new DiscountFreeProduct ("Deux achete Un gratuit",2,1);
        
        apple.setDiscount(discountOneGetOneFree);
        waterMelon.setDiscount(discountTwoGetOneFree);
        
        Basket basket = new Basket();
        
        basket.getBasketLines().add(new BasketLine(apple,4));
        basket.getBasketLines().add(new BasketLine(orange,3));
        basket.getBasketLines().add(new BasketLine(waterMelon,5));
        

        BasketService basketService = new BasketService(basket);

        double returnPrice = basketService.computeBasketPrice(); 

        // Assert
        double expectedPrice = 5.1; // 2*0.2 + 3*0.5 + 4*0.8
        assertEquals(expectedPrice, returnPrice, DELTA);

    }


}