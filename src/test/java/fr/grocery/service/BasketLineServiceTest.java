package fr.grocery.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.grocery.model.BasketLine;
import fr.grocery.model.DiscountFreeProduct;
import fr.grocery.model.Product;

public class BasketLineServiceTest {
	
    /**
     * Margin error for the comparison of double typed values.
     */
    private static final double DELTA = 0.000001;
    
    @Test
    public void testDiscountLineService() {

    	// create Products
        Product apple = new Product ("Apple", 0.20);
        Product orange = new Product("Orange", 0.50);
        Product waterMelon = new Product("WaterMelon", 0.80);
    	
    	
    	// create Discounts
        DiscountFreeProduct discountOneGetOneFree = new DiscountFreeProduct ("Un achete Un gratuit",1,1);
        DiscountFreeProduct discountTwoGetOneFree = new DiscountFreeProduct ("Deux achetes Un gratuit",2,1);        
        
    	
        // Set Discount to Products
        apple.setDiscount(discountOneGetOneFree);
        waterMelon.setDiscount(discountTwoGetOneFree);
        
    	// create BasketLines
        
        BasketLine appleBasketLine = new BasketLine (apple, 4); 
        BasketLine orangeBasketLine = new BasketLine (orange, 3); 
        BasketLine waterMelonBasketLine = new BasketLine (waterMelon, 5); 
        
        
    	//simulate Apple Discount
        double expectedPriceApple = 0.4; // 2*0.2       
        
        BasketLineService appleBasketLineService = new BasketLineService(appleBasketLine);
        double computedPriceApple = appleBasketLineService.computeBasketLinePrice();
        
        assertEquals(expectedPriceApple, computedPriceApple, DELTA);
        
    	//simulate WaterMelon Discount
        double expectedPriceWaterMelon = 3.2; // 0.8 * 2 + 0.8 *2
    	       
        BasketLineService waterMelonBasketLineService = new BasketLineService(waterMelonBasketLine);
        double computedPriceWaterMelon = waterMelonBasketLineService.computeBasketLinePrice();
        
        assertEquals(expectedPriceWaterMelon, computedPriceWaterMelon, DELTA);

        
    	//simulate Orange No Discount
        double expectedPriceOrange = 1.5; // 0.5 * 3
    	       
        BasketLineService orangeBasketLineService = new BasketLineService(orangeBasketLine);
        double computedPriceOrange = orangeBasketLineService.computeBasketLinePrice();
        
        assertEquals(expectedPriceOrange, computedPriceOrange, DELTA);

        
    }

}
