package fr.grocery.service;

import fr.grocery.model.DiscountFreeProduct;
import static java.lang.Math.min;

public class DiscountFreeProductService extends DiscountService {

	
    private DiscountFreeProduct discount;
    
	/**
	 * 
	 */
	public DiscountFreeProductService() {
		super();
	}

	/**
	 * @param discountFreeProduct
	 */
	public DiscountFreeProductService(DiscountFreeProduct discountFreeProduct) {
		super();
		this.discount = discountFreeProduct;
	}
	
    /**
     * Compute the price, with discount included 
     *
     * @return the price, with discount included
     */
	@Override
	public double getDiscountedPrice( double unitPrice, int quantity ) {
		if (this.discount ==null) {
			return unitPrice * quantity;
		} else {
	        // how many group are eligible for the offer
	        int numberProductGroup = quantity / ( this.discount.getBuyCount() + this.discount.getFreeCount());
	        // how many are remaining after groups : r
	        int remainingProductGroup = quantity % (this.discount.getBuyCount() + this.discount.getFreeCount());
	        // among the remainder how many is under buyCount : min
	        int min = min(remainingProductGroup, this.discount.getBuyCount());

	        int effectiveQuantity = this.discount.getBuyCount() * numberProductGroup + min;
	        
	        return unitPrice * effectiveQuantity;
	        
		}
	}
		
}
