package fr.grocery.service;

import fr.grocery.model.Basket;
import fr.grocery.model.BasketLine;


public class BasketService {
	

    private Basket basket;

        
    /**
	 * @param basket
	 */
	public BasketService(Basket basket) {
		this.basket = basket;
	}


    /**
     * Compute the total price of product in the basket, with special offer included 
     *
     * @return the basket line price with the special offers included
     */
    public double computeBasketPrice() {
    	
    	double basketPrice = 0.0;
    	
    	for (BasketLine basketLine : basket.getBasketLines() ) {
    		basketPrice += new BasketLineService(basketLine).computeBasketLinePrice();
    	}
    	
        return basketPrice;
    }

}
