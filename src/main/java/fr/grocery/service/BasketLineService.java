package fr.grocery.service;

import fr.grocery.model.BasketLine;
import fr.grocery.model.Discount;
import fr.grocery.model.DiscountFreeProduct;

public class BasketLineService {

    private BasketLine basketLine;

        
    /**
	 * @param basketLine
	 */
	public BasketLineService(BasketLine basketLine) {
		this.basketLine = basketLine;
	}


    /**
     * Compute the total price of product in the basket, with special offer included 
     *
     * @return the basket line price with the special offers included
     */
    public double computeBasketLinePrice() {
//    	return this.getQuantityApplyingDiscount() * basketLine.getProduct().getUnitPrice();
//    	Product product = basketLine.getProduct();
    	
    	Discount discountProduct = basketLine.getProduct().getDiscount();
    	
    	if (discountProduct instanceof DiscountFreeProduct )
    	{
    		DiscountFreeProduct discountFreeProduct = (DiscountFreeProduct) discountProduct;
        	double unitPrice = basketLine.getProduct().getUnitPrice();
        	int quantity = basketLine.getQuantity();
        	
        	DiscountFreeProductService discountFreeProductService = new DiscountFreeProductService(discountFreeProduct);
        	return discountFreeProductService.getDiscountedPrice(unitPrice, quantity);
        	
    	}
    	else
    	{
        	return basketLine.getQuantity() * basketLine.getProduct().getUnitPrice();
    	}
    	
    }


    /**
     * Get the number of item in the price computing, with special offer included 
     *
     * @return the basket line total price with the special offers included
     */
//    private int getQuantityApplyingDiscount() {
//    	return basketLine.getQuantity() ;
//    }

	
}
