package fr.grocery.service;

public abstract class DiscountService {

        
    /**
	 * 
	 */
	public DiscountService() {
	}

	
    /**
     * Compute the price, with discount included 
     *
     * @return the price, with discount included
     */
	public abstract double getDiscountedPrice( double unitPrice, int Quantity );
    

}
