package fr.grocery;

import fr.grocery.model.Basket;
import fr.grocery.model.BasketLine;
import fr.grocery.model.DiscountFreeProduct;
import fr.grocery.model.Product;
import fr.grocery.service.BasketService;

public class MainApp {
    public static void main(String[] args) {

        if (args.length < 3) {
            System.out.println("You must provide the number of item as following : \n" +
                    "\tjava -jar grocery-1.0-SNAPSHOT.jar <appleCount> <orangeCount> <waterMelonCount>");
            return;
        }

    	// create Products
        Product apple = new Product ("Apple", 0.20);
        Product orange = new Product("Orange", 0.50);
        Product waterMelon = new Product("WaterMelon", 0.80);
    	
    	
    	// create Discounts
        DiscountFreeProduct discountOneGetOneFree = new DiscountFreeProduct ("Un achete Un gratuit",1,1);
        DiscountFreeProduct discountTwoGetOneFree = new DiscountFreeProduct ("Deux achetes Un gratuit",2,1);        
        
    	
        // Set Discount to Products
        apple.setDiscount(discountOneGetOneFree);
        waterMelon.setDiscount(discountTwoGetOneFree);
        
 
        // get Count of product from command line 
        int appleCount = Integer.parseInt(args[0]);
        int orangeCount = Integer.parseInt(args[1]);
        int waterMelonCount = Integer.parseInt(args[2]);

    	// create BasketLines
        
        BasketLine appleBasketLine = new BasketLine (apple, appleCount); 
        BasketLine orangeBasketLine = new BasketLine (orange, orangeCount); 
        BasketLine waterMelonBasketLine = new BasketLine (waterMelon, waterMelonCount); 
       

        // create Basket and add basket lines
        Basket basket = new Basket();
        
        basket.getBasketLines().add(appleBasketLine);
        basket.getBasketLines().add(orangeBasketLine);
        basket.getBasketLines().add(waterMelonBasketLine);

        
        //get 
        
        BasketService basketService = new BasketService(basket);

        double basketPrice = basketService.computeBasketPrice(); 
        

        System.out.println(String.format("The total price of basket is : %s", basketPrice));

    }
}
