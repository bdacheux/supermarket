package fr.grocery.model;

import java.io.Serializable;


import java.util.UUID;


/**
 * Object representing the product in the grocery.
 */
public class Product implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
     * The id of the product
     */
	private UUID idProduct; 
	

	/**
     * The name of the product
     */
    private String name;

    /**
     * The price per product
     */
    private double unitPrice;

    /**
     * The discount applied to this product
     */
    private Discount discount;


    /**
	 * 
	 */
	public Product() {
    	this.idProduct = UUID.randomUUID();    	
	}

    /**
     * Creates a product
     *
     * @param name The name of the product
     */
    public Product(String name) {
    	this.idProduct = UUID.randomUUID();
        this.name = name;
        this.unitPrice = 0.0;
    }

	/**
     * Creates a product
     *
     * @param name         The name of the product
     * @param pricePerUnit The price per unit
     */
    public Product(String name, double unitPrice) {
    	this.idProduct = UUID.randomUUID();
        this.name = name;
        this.unitPrice = unitPrice;          
        
        System.out.println(String.format("Creation du produit : %s", toString()));
    }

    /**
	 * @return the idProduct
	 */
	public UUID getIdProduct() {
		return idProduct;
	}	

	/**
	 * @param idProduct the idProduct to set
	 */
	public void setIdProduct(UUID idProduct) {
		this.idProduct = idProduct;
	}

	
    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the unitPrice
	 */
	public double getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	

	/**
	 * @return the discount
	 */
	public Discount getDiscount() {
		return discount;
	}

	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(Discount discount) {
		this.discount = discount;

        System.out.println(String.format("Modification de promotion sur produit : %s", toString()));
	}

	
	

	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", name=" + name + ", unitPrice=" + unitPrice + ", discount="
				+ discount + "]";
	}

	/**
     * Compute the price of the items based on the quantity, price per unit and the special offer.
     *
     * @return the price of the items
     */
    public double getPriceForNProducts (int quantity) {

    	return quantity * this.getUnitPrice();
/*    	
        // we must apply euclidean division (a = b*q + r)

        // how many group is eligible for the offer : q
        int q = getQuantity() / (getOffer().getBuyCount() + getOffer().getFreeCount());
        // how many is remaining : r
        int r = getQuantity() % (getOffer().getBuyCount() + getOffer().getFreeCount());
        // among the remainder how many is under buyCount : min
        int min = min(r, getOffer().getBuyCount());

        int effectiveQuantity = getOffer().getBuyCount() * q + min;

        return getUnitPrice() * effectiveQuantity;
*/        
    }


/*
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
*/

/*    
    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }
*/
    
}
