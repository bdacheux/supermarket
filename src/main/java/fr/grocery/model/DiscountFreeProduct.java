package fr.grocery.model;

public class DiscountFreeProduct extends Discount {
	
    /**
     * The required number of product to buy to get free products
     */
    private int buyCount;
    /**
     * The number of free product  when the buy count is reached
     */
    private int freeCount;
    

	/**
	 * 
	 */
	public DiscountFreeProduct() {
		super();
	}
	

	/**
	 * @param discountName
	 * @param buyCount
	 * @param freeCount
	 */
	public DiscountFreeProduct(String discountName, int buyCount, int freeCount) {
		super(discountName);
		this.buyCount = buyCount;
		this.freeCount = freeCount;
		
        System.out.println(String.format("Creation de la promotion : %s", toString()));
	}


	/**
	 * @return the buyCount
	 */
	public int getBuyCount() {
		return buyCount;
	}


	/**
	 * @param buyCount the buyCount to set
	 */
	public void setBuyCount(int buyCount) {
		this.buyCount = buyCount;
	}


	/**
	 * @return the freeCount
	 */
	public int getFreeCount() {
		return freeCount;
	}


	/**
	 * @param freeCount the freeCount to set
	 */
	public void setFreeCount(int freeCount) {
		this.freeCount = freeCount;
	}


	@Override
	public String toString() {
		return "DiscountFreeProduct [buyCount=" + buyCount + ", freeCount=" + freeCount + ", idDiscount=" + idDiscount
				+ ", discountName=" + discountName + "]";
	}


}
