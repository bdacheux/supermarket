package fr.grocery.model;

import java.util.UUID;

/**
 * Class representing the discount.
 */
public abstract class Discount {
	
    /**
     * The id of the discount
     */
	protected UUID idDiscount; 
	
    /**
     * The name of the discount
     */
	String discountName;
	

    /**
	 * 
	 */
	public Discount() {
		this.idDiscount = UUID.randomUUID();
	}
	

	/**
	 * @param discountName
	 */
	public Discount(String discountName) {
		this.idDiscount = UUID.randomUUID();
		this.discountName = discountName;
	}


	/**
	 * @return the idDiscount
	 */
	public UUID getIdDiscount() {
		return idDiscount;
	}


	/**
	 * @param idDiscount the idDiscount to set
	 */
	public void setIdDiscount(UUID idDiscount) {
		this.idDiscount = idDiscount;
	}


	/**
	 * @return the discountName
	 */
	public String getDiscountName() {
		return discountName;
	}


	/**
	 * @param discountName the discountName to set
	 */
	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}


	
	
	@Override
	public String toString() {
		return "Discount [idDiscount=" + idDiscount + ", discountName=" + discountName + "]";
	}
	
	
	

}
