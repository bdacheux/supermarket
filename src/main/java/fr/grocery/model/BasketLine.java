package fr.grocery.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * Object representing the basket line in a basket
 */
public class BasketLine  implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * The id of the product line
     */
	private UUID idBasketLine; 
	
    /**
     * The product of the line
     */
    private Product product;
    
    /**
     * The number of products
     */
    private int quantity;

       
    
	/**
	 * @param product
	 * @param quantity
	 */
	public BasketLine(Product product, int quantity) {
		this.idBasketLine = UUID.randomUUID();
		this.product = product;
		this.quantity = quantity;
		
        System.out.println(String.format("Creation de la ligne de panier : %s", toString()));

	}


	
	/**
	 * @return the idProductLine
	 */
	public UUID getIdProductLine() {
		return idBasketLine;
	}


    /**
	 * @param idProductLine the idProductLine to set
	 */
	public void setIdProductLine(UUID idProductLine) {
		this.idBasketLine = idProductLine;
	}




	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}


	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}


	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}


	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}



	@Override
	public String toString() {
		return "BasketLine [idBasketLine=" + idBasketLine + ", product=" + product + ", quantity=" + quantity + "]";
	}




}
