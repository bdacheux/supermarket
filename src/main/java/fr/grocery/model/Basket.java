package fr.grocery.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Contains a Basket of Product of the Grocery 
 */
public class Basket implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * The id of the basket
     */
	private UUID idBasket; 

	
    /**
     * The list of BasketLine in the basket
     */
    private ArrayList<BasketLine> basketLines; 

    
    
    /**
	 * 
	 */
	public Basket() {
		this.idBasket = UUID.randomUUID();
		this.basketLines = new ArrayList<BasketLine>();
		
        System.out.println(String.format("Creation du panier : %s", toString()));
	}


	/**
	 * @return the idBasket
	 */
	public UUID getIdBasket() {
		return idBasket;
	}


	/**
	 * @param idBasket the idBasket to set
	 */
	public void setIdBasket(UUID idBasket) {
		this.idBasket = idBasket;
	}


	/**
	 * @return the basketLines
	 */
	public ArrayList<BasketLine> getBasketLines() {
		return basketLines;
	}


	/**
	 * @param basketLines the basketLines to set
	 */
	public void setBasketLines(ArrayList<BasketLine> basketLines) {
		this.basketLines = basketLines;
	}


	@Override
	public String toString() {
		return "Basket [idBasket=" + idBasket + ", basketLines=" + basketLines + "]";
	}

	
	
	
}
